package net.waqassiddiqi.itunes.ui;

import java.util.ArrayList;
import java.util.List;

import net.waqassiddiqi.itunes.R;
import net.waqassiddiqi.itunes.adapter.FeedListAdapter;
import net.waqassiddiqi.itunes.business.FetchFeedTask;
import net.waqassiddiqi.itunes.business.FetchFeedTask.OnCompleteListener;
import net.waqassiddiqi.itunes.business.FetchFeedTask.OnExceptionListener;
import net.waqassiddiqi.itunes.dao.FeedDao;
import net.waqassiddiqi.itunes.model.FeedEntry;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnCompleteListener<List<FeedEntry>>, OnExceptionListener {

	FeedListAdapter feedListAdapter;
	FetchFeedTask taskFeedFetch;	
	ProgressDialog progressDialog;
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);		
		
		taskFeedFetch = (FetchFeedTask) getLastNonConfigurationInstance();
		
		if(taskFeedFetch == null) {
			List<FeedEntry> list = new FeedDao().getAll(); 
			if(list != null && list.size() > 0) {
				bindView(list);
			} else {			
				fetchFeeds();
			}
		} else {
			showProgressDialog();
			taskFeedFetch.attachCompleteListener(this);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(0, 1, 1, "Refresh Feeds");
		
		return super.onCreateOptionsMenu(menu); 
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case 1:
				fetchFeeds();
				return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public Object onRetainNonConfigurationInstance() {
		if(taskFeedFetch != null) {
			taskFeedFetch.detach();			
		}
		
		return taskFeedFetch;
	}
	
	private void showProgressDialog() {
		progressDialog = ProgressDialog.show(this, "", "Loading...", true);
	}
	
	private void fetchFeeds() {
		showProgressDialog();
		taskFeedFetch = new FetchFeedTask(
				"http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topsongs/limit=10/xml");
		
		taskFeedFetch.setOnCompleteListener(this);
		taskFeedFetch.setOnExceptionListener(this);
		taskFeedFetch.execute();
	}
	
	private void bindView(List<FeedEntry> feedEntryList) {
		
		ExpandableListView listView = (ExpandableListView) findViewById(R.id.lstFeeds);
		
		feedListAdapter = new FeedListAdapter(this, new ArrayList<FeedEntry>());
		
		for(int i=0; i<feedEntryList.size(); i++) {
			feedListAdapter.addItem(feedEntryList.get(i));
		}
			
		listView.setAdapter(feedListAdapter);
	}

	@Override
	public void onComplete(List<FeedEntry> feeds) {
		bindView(feeds);
		
		if(progressDialog != null)
			progressDialog.dismiss();
		taskFeedFetch = null;
	}

	@Override
	public void onException(Exception exception) {
		if(progressDialog != null)
			progressDialog.dismiss();
				
		Toast.makeText(MainActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
		taskFeedFetch = null;
	}
}
