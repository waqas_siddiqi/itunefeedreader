package net.waqassiddiqi.itunes.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class WebViewActivity extends Activity {
	
	//private WebView webview;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_PROGRESS);
		
		WebView webview = new WebView(this);
		setContentView(webview);
		
		Bundle extras = getIntent().getExtras();
		if(extras != null && extras.containsKey("url") && webview != null) {
			
			final Activity activity = this;
			
			webview.getSettings().setJavaScriptEnabled(true);			
			webview.setWebChromeClient(new WebChromeClient() {
				public void onProgressChanged(WebView view, int progress) {					
					activity.setProgress(progress * 100);
				}});			
			webview.setWebViewClient(new WebViewClient() {
				public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
					Toast.makeText(activity, "Error: " + description, Toast.LENGTH_SHORT).show();
				}});
			
			webview.loadUrl(extras.getString("url"));
			
		} else {
			Toast.makeText(this, "Invalid url", Toast.LENGTH_LONG).show();
			finish();
		}
	}
	
}
