package net.waqassiddiqi.itunes.business;

import java.net.URL;
import java.util.List;

import net.waqassiddiqi.itunes.Constant;
import net.waqassiddiqi.itunes.iTuneApp;
import net.waqassiddiqi.itunes.dao.FeedDao;
import net.waqassiddiqi.itunes.model.FeedEntry;
import net.waqassiddiqi.itunes.parser.FeedParser;
import net.waqassiddiqi.itunes.utils.NetworkUtils;

import android.os.AsyncTask;
import android.util.Log;

public class FetchFeedTask extends AsyncTask<Void, Void, List<FeedEntry>> {
	
	private String feedUrl;
	private FeedDao feedDao;
	private Exception exception;
	
	public static interface OnCompleteListener<T> {
		public void onComplete(T feeds);
	}
	
	private OnCompleteListener<List<FeedEntry>> onCompleteListener;
	public void setOnCompleteListener(OnCompleteListener<List<FeedEntry>> onCompleteListener) {
		this.onCompleteListener = onCompleteListener;
	}
	
	public static interface OnExceptionListener {
		public void onException(Exception exception);
	}
	
	private OnExceptionListener onExceptionListener;
	public void setOnExceptionListener(OnExceptionListener onExceptionListener) {
		this.onExceptionListener = onExceptionListener;
	}
	
	public FetchFeedTask(String feedUrl) {
		super();
		this.feedUrl = feedUrl;
		this.feedDao = new FeedDao();
	}

	@Override
	protected List<FeedEntry> doInBackground(Void... params) {
		try {
			
			if(!NetworkUtils.isOnline(iTuneApp.getContext()))
				throw new Exception("Internet is not available");
			
			if(this.feedUrl != null && this.feedUrl.trim().length() > 0) {
				return FeedParser.parse(new URL(feedUrl).openConnection().getInputStream());
			}
			
		} catch (Exception e) {
			this.exception = e;
			Log.e(Constant.TAG, e.getMessage(), e);
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(List<FeedEntry> feedEntryList) {
		if(feedEntryList != null) {
			
			feedDao.deleteAll();
			
			for(FeedEntry entry : feedEntryList) {
				feedDao.insert(entry);
			}
			
			if (this.onCompleteListener != null)
				this.onCompleteListener.onComplete(feedEntryList);
			
		} else {
			if (this.onExceptionListener != null)
				this.onExceptionListener.onException(
						this.exception == null ?
								new Exception("An error has occured while parsing feeds") : this.exception
						);
		}
	}
	
	public void detach() {
		this.onCompleteListener = null;
		this.onExceptionListener = null;
	}
	
	public void attachCompleteListener(OnCompleteListener<List<FeedEntry>> listener) {
		this.onCompleteListener = listener;
	}
	
	public void attachExceptionListener(OnExceptionListener listener) {
		this.onExceptionListener = listener;
	}
}
