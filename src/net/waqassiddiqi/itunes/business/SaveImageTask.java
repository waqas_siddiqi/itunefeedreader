package net.waqassiddiqi.itunes.business;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import com.fedorvlasov.lazylist.Utils;

import net.waqassiddiqi.itunes.Constant;
import net.waqassiddiqi.itunes.iTuneApp;
import net.waqassiddiqi.itunes.utils.NetworkUtils;

import android.os.AsyncTask;
import android.util.Log;

public class SaveImageTask extends AsyncTask<Void, Void, Boolean> {

	private String imageUrl;
	private File savedFile;
	private Exception exception;
	
	public static interface OnCompleteListener<T> {
		public void onComplete(T isSaved);
	}
	
	private OnCompleteListener<Boolean> onCompleteListener;
	public void setOnCompleteListener(OnCompleteListener<Boolean> onCompleteListener) {
		this.onCompleteListener = onCompleteListener;
	}
	
	public static interface OnExceptionListener {
		public void onException(Exception exception);
	}
	
	private OnExceptionListener onExceptionListener;
	public void setOnExceptionListener(OnExceptionListener onExceptionListener) {
		this.onExceptionListener = onExceptionListener;
	}
	
	public SaveImageTask(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	@Override
	protected void onPreExecute() {
		File cacheDir;
		
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			cacheDir = new File(android.os.Environment.getExternalStorageDirectory(), "Download");
		else
            cacheDir = iTuneApp.getContext().getCacheDir();
		
		if(!cacheDir.exists())
			cacheDir.mkdirs();
		
		String[] arr = imageUrl.split("/");
		savedFile = new File(cacheDir, arr[arr.length - 1]);
	}
	
	@Override
	protected Boolean doInBackground(Void... v) {
		try {
			
			if(!NetworkUtils.isOnline(iTuneApp.getContext()))
				throw new Exception("Internet is not available");
			
			InputStream inputStream = new URL(imageUrl).openConnection().getInputStream();
			OutputStream outputStream = new FileOutputStream(savedFile);
			Utils.CopyStream(inputStream, outputStream);
			outputStream.close();
			
			return true;
			
		} catch (Exception e) {
			this.exception = e;
			Log.e(Constant.TAG, e.getMessage(), e);
		}
		
		return false;
	}
	
	@Override
	protected void onPostExecute(Boolean isSaved) {
		if(isSaved) {
			if (this.onCompleteListener != null)
				this.onCompleteListener.onComplete(isSaved);
		} else {
			if (this.onExceptionListener != null)
				this.onExceptionListener.onException(
						this.exception == null ?
								new Exception("An error has occured while saving image") : this.exception
						);
		}
	}
}