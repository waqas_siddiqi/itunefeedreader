package net.waqassiddiqi.itunes;

import org.acra.ACRA;
import org.acra.annotation.ReportsCrashes;

import android.app.Application;
import android.content.Context;


@ReportsCrashes(formKey = "dFh2Tk1qY01FendWcXAwZWh1R0g3QkE6MQ")
public class iTuneApp extends Application {
	private static iTuneApp instance;
	
	public iTuneApp() {
		instance = this;
	}
	
	public static Context getContext() {
		return instance.getApplicationContext();
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		ACRA.init(this);
	}
}
