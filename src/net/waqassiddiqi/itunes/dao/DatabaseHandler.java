package net.waqassiddiqi.itunes.dao;

import net.waqassiddiqi.itunes.iTuneApp;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "itunesAppDb";
	
	public DatabaseHandler() {
        super(iTuneApp.getContext(), DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(FeedDao.CREATE_FEED_TABLE_SQL);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + FeedDao.FEED_TABLE);		
		onCreate(db);
	}
}
