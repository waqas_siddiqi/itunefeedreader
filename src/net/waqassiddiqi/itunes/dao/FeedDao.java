package net.waqassiddiqi.itunes.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import net.waqassiddiqi.itunes.Constant;
import net.waqassiddiqi.itunes.model.FeedEntry;

public class FeedDao {
	public static final String FEED_TABLE = "feed_tab";
	public static final String FEED_ID = "id";
	public static final String FEED_TITLE = "title";
	public static final String FEED_ALBUM_IMAGE_URL = "album_image";
	public static final String FEED_SONG_URL = "song_web_url";
	
	public static final String CREATE_FEED_TABLE_SQL = "CREATE TABLE " + FEED_TABLE + "(" +
			FEED_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
			FEED_TITLE + " TEXT, " +
			FEED_ALBUM_IMAGE_URL + " TEXT, " +
			FEED_SONG_URL + " TEXT)";
	
	public List<FeedEntry> getAll() {
		List<FeedEntry> feedList = new ArrayList<FeedEntry>();
		SQLiteDatabase db = new DatabaseHandler().getWritableDatabase();
		try {
			
			Cursor cursor = db.query(FEED_TABLE, null, null, null, null, null, null);
			while(cursor.moveToNext()) {
				FeedEntry entry = new FeedEntry();
				entry.setId(cursor.getLong(0));
				entry.setTitle(cursor.getString(1));
				entry.setAlbumImageUrl(cursor.getString(2));
				entry.setSongWebUrl(cursor.getString(3));
				
				feedList.add(entry);
			}
			
		} catch (Exception e) {
			Log.e(Constant.TAG, e.getMessage(), e);
		} finally {
			db.close();
		}
		
		return feedList;
	}
	
	public long insert(FeedEntry feedEntry) {
		long id = -1;
		SQLiteDatabase db = new DatabaseHandler().getWritableDatabase();
		try {
			ContentValues values = new ContentValues();
			values.put(FEED_TITLE, feedEntry.getTitle());
		    values.put(FEED_ALBUM_IMAGE_URL, feedEntry.getAlbumImageUrl());
		    values.put(FEED_SONG_URL, feedEntry.getSongWebUrl());
		    
		    id = db.insert(FEED_TABLE, null, values);
		} catch (Exception e) {
			Log.e(Constant.TAG, e.getMessage(), e);
		} finally {
			db.close();
		}	    
	    
	    return id;
	}
	
	public void deleteAll() {
		SQLiteDatabase db = new DatabaseHandler().getWritableDatabase();
		db.delete(FEED_TABLE, null, null);
		db.close();
	}
}
