package net.waqassiddiqi.itunes.adapter;

import java.util.ArrayList;
import java.util.List;

import com.fedorvlasov.lazylist.ImageLoader;

import net.waqassiddiqi.itunes.R;
import net.waqassiddiqi.itunes.business.SaveImageTask;
import net.waqassiddiqi.itunes.business.SaveImageTask.OnCompleteListener;
import net.waqassiddiqi.itunes.business.SaveImageTask.OnExceptionListener;
import net.waqassiddiqi.itunes.model.FeedEntry;
import net.waqassiddiqi.itunes.ui.WebViewActivity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FeedListAdapter extends BaseExpandableListAdapter {

	private Context mContext;
	private List<FeedEntry> mGroupList;
	private ImageLoader mImgLoader;
	
	public FeedListAdapter(Context context, List<FeedEntry> groupList) {
		mContext = context;
		mGroupList = groupList;
		mImgLoader = new ImageLoader(context);
	}
	
	public void addItem(FeedEntry feedEntry) {
		mGroupList.add(feedEntry);
		ArrayList<FeedEntry> childList = new ArrayList<FeedEntry>();
		childList.add(feedEntry);
	}
	
	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return mGroupList.get(groupPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, 
			View convertView, ViewGroup parent) {

		if (convertView == null) {
			LayoutInflater infalInflater = 
					(LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.listview_menu_layout, null);
		}				
		
		if(getChild(groupPosition, childPosition) instanceof FeedEntry) {
			final FeedEntry feedEntry = (FeedEntry) getChild(groupPosition, childPosition);
			TextView txtDownloadImage = (TextView) convertView.findViewById(R.id.txtDownloadImage);
			if(txtDownloadImage != null) {
				txtDownloadImage.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Toast.makeText(mContext, "Downloading image...", Toast.LENGTH_LONG).show();
						
						SaveImageTask taskSaveImage = new SaveImageTask(feedEntry.getAlbumImageUrl());												
						taskSaveImage.setOnCompleteListener(new OnCompleteListener<Boolean>() {							
							@Override
							public void onComplete(Boolean isSaved) {								
								Toast.makeText(mContext, "Image downloaded successfully", Toast.LENGTH_LONG).show();
							}
						});
						
						taskSaveImage.setOnExceptionListener(new OnExceptionListener() {							
							@Override
							public void onException(Exception exception) {
								Toast.makeText(mContext, exception.getMessage(), Toast.LENGTH_LONG).show();
							}
						});
						
						taskSaveImage.execute();
					}
				});
			}
			
			TextView txtMoreInfo = (TextView) convertView.findViewById(R.id.txtMoreInfo);
			if(txtMoreInfo != null) {
				txtMoreInfo.setOnClickListener(new OnClickListener() {					
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(mContext, WebViewActivity.class);
						intent.putExtra("url", feedEntry.getSongWebUrl());
						mContext.startActivity(intent);
					}
				});
			}
		}
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 1;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return mGroupList.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return mGroupList.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.group_layout, null);
		}
		
		if(getGroup(groupPosition) instanceof FeedEntry) {
			FeedEntry entry = (FeedEntry) getGroup(groupPosition);
			
			TextView txtSongTitle = (TextView) convertView.findViewById(R.id.txtSongTitle);
			if(txtSongTitle != null)
				txtSongTitle.setText(entry.getTitle());
			
			ImageView imgAlbumCover = (ImageView) convertView.findViewById(R.id.imgAlbumCover);
			if(imgAlbumCover != null) {
				mImgLoader.DisplayImage(entry.getAlbumImageUrl(), imgAlbumCover);
			}
		}				
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		return true;
	}

}
