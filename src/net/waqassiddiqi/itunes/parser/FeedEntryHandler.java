package net.waqassiddiqi.itunes.parser;

import java.util.ArrayList;
import java.util.List;

import net.waqassiddiqi.itunes.model.FeedEntry;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


public class FeedEntryHandler extends DefaultHandler {	
	private List<FeedEntry> feedList;
	public List<FeedEntry> getFeedList(){
		return this.feedList;
	}
	
	private FeedEntry currentFeed;		
	private StringBuilder builder;	
	private boolean isImageEntry = false;
	
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		feedList = new ArrayList<FeedEntry>();
		builder = new StringBuilder();
	}

	@Override
	public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
		
		super.startElement(uri, localName, name, attributes);
		
		if (localName.equalsIgnoreCase("entry")) {
			this.currentFeed = new FeedEntry();
		} else if (this.currentFeed != null && localName.equalsIgnoreCase("image")) {
			if(attributes.getValue("height").equals("60")) {
				isImageEntry = true;
			}
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		builder.append(ch, start, length);
	}

	@Override
	public void endElement(String uri, String localName, String name) throws SAXException {
		super.endElement(uri, localName, name);
		
		if (this.currentFeed != null){
			if (localName.equalsIgnoreCase("title")) {				
				currentFeed.setTitle(builder.toString().replaceAll("[\t\n]+", ""));				
			} else if (localName.equalsIgnoreCase("id")) {				
				currentFeed.setSongWebUrl(builder.toString().replaceAll("[\t\n]+", ""));
			} else if (localName.equalsIgnoreCase("image") && isImageEntry) {				
				//Log.d(Constant.TAG, builder.toString().replaceAll("[\t\n]+", ""));
				currentFeed.setAlbumImageUrl(builder.toString().replaceAll("[\t\n]+", ""));
				isImageEntry = false;
			} else if (localName.equalsIgnoreCase("entry")) {				
				feedList.add(currentFeed);
				
			}
			
			builder.setLength(0);	
		}
	}
}