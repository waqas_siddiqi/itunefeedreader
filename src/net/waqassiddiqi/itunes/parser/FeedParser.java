package net.waqassiddiqi.itunes.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import net.waqassiddiqi.itunes.model.FeedEntry;

public class FeedParser {
	public static List<FeedEntry> parse(InputStream inputStream) throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory saxFactory = SAXParserFactory.newInstance();
		SAXParser saxParser;		
		FeedEntryHandler feedEntryHandler;
		
		saxParser = saxFactory.newSAXParser();		
		feedEntryHandler = new FeedEntryHandler();
		
		saxParser.parse(inputStream, feedEntryHandler);
		return feedEntryHandler.getFeedList();
	}
}