package net.waqassiddiqi.itunes.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtils {
	public static boolean isOnline(Context contetx) {		
		ConnectivityManager cm = 
				(ConnectivityManager) contetx.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		
		if (netInfo != null && netInfo.isConnected()) {		
	        return true;
	    }
		
		return false;
	}
}
