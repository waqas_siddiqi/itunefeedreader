package net.waqassiddiqi.itunes.model;

public class FeedEntry {
	private long id;
	public void setId(long id) {
		this.id = id;
	}
	public long getId() {
		return this.id;
	}
	
	private String title;
	public void setTitle(String title) { 
		this.title = title; 
	}
	public String getTitle() { 
		return this.title; 
	}
	
	private String albumImageUrl;
	public void setAlbumImageUrl(String albumImageUrl) {
		this.albumImageUrl = albumImageUrl;
	}	
	public String getAlbumImageUrl() {
		return this.albumImageUrl;
	}
	
	private String songWebUrl;
	public void setSongWebUrl(String songWebUrl) {
		this.songWebUrl = songWebUrl;
	}
	public String getSongWebUrl() {
		return this.songWebUrl;
	}
}
